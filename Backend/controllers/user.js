const User = require('../models/user');
const bcryptjs = require('bcryptjs');
const { updateUserValidation } = require('../validation')
const controller = require('../controllers/auth')

exports.getUsers = async (req, res) => {
  try {
    const users = await User.find().sort({"lastname": 1})
    const count = await User.find().countDocuments()
    Data = {name: users.name, lastname: users.lastname, usertype: users.usertype, username: users.username}
    
    res.status(200)
      .json({ message: 'Prikupljeni korisnici', Data: users, count: count })
  }
  catch (err) {
    res.json({ success: false });
    console.log(err);
  }
};
exports.getAllRegularUsers = async (req, res, next) => {
  try {
    const users = await User.find()

    const usersRegular = await User.find({"usertype": 0})
    res.status(200)
      .json({ Data: usersRegular })
  }
  catch (err) {
    res.json({ Success: false });
    console.log(err);
  }
};
exports.getUserById = async (req, res) => {
  try {
    const user = await User.findById(req.params.userId);
    Data = { name: user.name, lastname: user.lastname, address: user.address, email: user.email, password: user.password, number: user.number }
    res.json({ message: 'pribavljen je korisnik', user: user, Data })
  }
  catch (err) {
    res.json({ success: false });
    console.log(err);
  }
}

exports.findByEmail = async (req, res, next) => {
  const userEmail = req.params.userEmail;

  try {
    const user = await User.findOne({ email: userEmail })
    res.json({ message: 'pribavljen je korisnik', user: user })
  }
  catch (err) {
    res.json({ success: false });
    console.log(err);
  }
}

exports.updateUser = async (req, res, next) => {
  const { error } = updateUserValidation(req.body);
  if (error)
    return res.status(400).send(error.details[0].message);
  const salt = await bcryptjs.genSalt(10);

  try {
    const user = await User.findById(req.body.userId)
    const oldPass = req.body.oldPass
    const newPass = req.body.newPass

    const goodPassword = controller.checkPassword(oldPass, user.password)
    if (goodPassword) {
      const hashedPw = await bcryptjs.hash(req.body.newPass, salt);
      var myquery = { _id: req.body.userId };
      var newvalues = { $set: { password: hashedPw } };
      const savedUser = await User.updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("1 document updated");
      });
    }
    res.json({ Success: true });
  }
  catch (err) {
    res.json({ success: false });
    console.log(err);
  }
};

exports.deleteUser = async (req, res, next) => {
  const user = await User.findByIdAndRemove(req.params.userId)
  try {
    res.json({ Success: true, message: "Obrisano!" });
  }
  catch (err) {
    res.json({ success: false });
    console.log(err);
  }
}

